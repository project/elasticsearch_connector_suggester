<?php

namespace Drupal\elasticsearch_connector_suggester\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\elasticsearch_connector\Event\FieldMappingEvent;
use Drupal\elasticsearch_connector\Event\IndexParamsEvent;
use Drupal\elasticsearch_connector\Event\QueryParamsEvent;
use Drupal\elasticsearch_connector_suggester\Event\RequestNameEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Utility\FieldsHelperInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class DefaultSubscriber for elasticsearch connector suggester.
 */
class DefaultSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new DefaultSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager interface.
   * @param \Drupal\search_api\Utility\FieldsHelperInterface $fieldsHelper
   *   Fields helper interface.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match interface.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher interface.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly FieldsHelperInterface $fieldsHelper,
    protected readonly RouteMatchInterface $routeMatch,
    protected readonly EventDispatcherInterface $eventDispatcher,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[FieldMappingEvent::class] = [
      'onFieldMapping',
    ];
    $events[QueryParamsEvent::class] = [
      'onElasticsearchBuildQuery',
    ];
    $events[IndexParamsEvent::class] = [
      'onElasticsearchBuildParams',
    ];
    $events[ConfigEvents::SAVE] = [
      'onConfigSave',
    ];
    $events[SearchApiEvents::PROCESSING_RESULTS] = [
      'onSearchApiProcessingResults',
    ];

    return $events;
  }

  /**
   * Called on elasticsearch FieldMappingEvent event.
   *
   * Change suggester field type to completion.
   *
   * @param \Drupal\elasticsearch_connector\Event\FieldMappingEvent $event
   *   The event.
   */
  public function onFieldMapping(Event $event) {
    $index = $event->getField()->getIndex();
    $fields = $this->getFields($index);
    if (empty($fields)) {
      return;
    }
    $field = array_shift($fields);
    $field_id = $event->getField()->getFieldIdentifier();
    if ($field_id != $field) {
      return;
    }

    // Get suggester analyzer.
    $analizer = 'keyword';
    // Load search for index.
    /** @var \Drupal\search_api_autocomplete\SearchInterface[] $searches */
    $searches = $this->entityTypeManager
      ->getStorage('search_api_autocomplete_search')
      ->loadByProperties([
        'index_id' => $index->id(),
      ]);
    /** @var \Drupal\search_api_autocomplete\SearchInterface $search */
    $search = array_shift($searches);
    if (is_a($search, 'Drupal\search_api_autocomplete\Entity\Search')) {
      // Get suggester for search.
      /** @var \Drupal\search_api_autocomplete\Suggester\SuggesterInterface $suggester */
      $suggester = $search->getSuggester('elastic_live_results');
      if (is_a($suggester, 'Drupal\search_api_autocomplete\Suggester\SuggesterInterface')) {
        // Get suggester analyzer.
        $analizer = $suggester->getConfiguration()['analyzer'] ?? 'keyword';
      }
    }

    // Set field type to completion.
    $param = [
      'type' => 'completion',
      'analyzer' => $analizer,
      'max_input_length' => 1000,
    ];

    $event->setParam($param);
  }

  /**
   * Called on elasticsearch_connector.build_params event.
   *
   * Add suggester value to index element.
   *
   * @param \Drupal\elasticsearch_connector\Event\IndexParamsEvent $event
   *   The event.
   */
  public function onElasticsearchBuildParams(Event $event) {
    $index_name = $event->getIndexName();
    $index = $this->loadIndexFromIndexName($index_name);
    if ($index === NULL) {
      return;
    }
    $fields = $this->getFields($index);
    if (empty($fields)) {
      return;
    }

    $params = $event->getParams();
    foreach ($fields as $field) {
      foreach ($params['body'] as &$body) {
        if (!isset($body[$field])) {
          continue;
        }
        // Remove extra array.
        $text = $body[$field] ?? '';
        if (is_array($text)) {
          $text = current($text);
        }
        $body[$field] = $this->getSuggesterValues($index, $text);
      }
    }

    $event->setParams($params);
  }

  /**
   * Called on elasticsearch_connector QueryParamsEvent.
   *
   * Replace query to suggester request.
   *
   * @param \Drupal\elasticsearch_connector\Event\QueryParamsEvent $event
   *   The event.
   */
  public function onElasticsearchBuildQuery(Event $event) {
    $params = $event->getParams();
    $index_name = $params["index"] ?? NULL;
    if ($index_name === NULL) {
      return;
    }
    $index = $this->loadIndexFromIndexName($index_name);
    $fields = $this->getFields($index);
    if (empty($fields)) {
      return;
    }
    $field_name = $params['body']['query']['query_string']['fields'][0] ?? NULL;
    if ($field_name === NULL) {
      return;
    }
    // Check if autocomplete use the query field name
    foreach ($fields as $field) {
      if (strpos($field_name, $field) !== FALSE) {
        $autocomplete_name = $field;
        break;
      }
    }
    if (!isset($autocomplete_name)) {
      // Query field name not contains autocomplete field name.
      return;
    }

    // Get keys from query.
    $keys = $params['body']['query']['query_string']['query'] ?? '';
    if (empty($keys)) {
      return;
    }
    // Remove not needed words and characters.
    $prefix = preg_replace('/( AND|~)/', '', $keys);

    // The autocomplete route name.
    $route_name = 'search_api_autocomplete.autocomplete';
    /** @var \Drupal\elasticsearch_connector_suggester\Event\RequestNameEvent $name_event */
    $name_event = new RequestNameEvent($route_name);
    // Allow modification route name via an event.
    $this->eventDispatcher->dispatch($name_event);
    $route_name = $name_event->getName();

    // If route is autocomplete skip duplicates.
    $skip_duplicates = $this->routeMatch->getRouteName() === $route_name;
    // @todo maybe needs to configurable in the future.
    // Set size to 10 for autocomplete.
    $size = $skip_duplicates ? 10 : NULL;

    // Set suggest request.
    $params['body'] = [
      'suggest' => [
        'autocomplete' => [
          'prefix' => $prefix,
          'completion' => [
            'field' => $field,
            'skip_duplicates' => $skip_duplicates,
          ],
        ],
      ],
    ];
    if ($size !== NULL) {
      $params['body']['suggest']['autocomplete']['completion']['size'] = $size;
    }

    $event->setParams($params);
  }

  /**
   * Called on config.save event.
   *
   * Update elasticsearch index mapping.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The event.
   */
  public function onConfigSave(Event $event) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $event->getConfig();
    $name = $config->getName();
    if (!preg_match('/^search_api_autocomplete.search.*$/', $name)) {
      return;
    }
    // Save index to update mapping.
    if ($index = $this->entityTypeManager->getStorage('search_api_index')
      ->load($config->get('index_id'))) {
      $index->save();
    }
  }

  /**
   * Called on search_api.processing_results event.
   *
   * Add suggester results.
   *
   * @param \Drupal\search_api\Event\ProcessingResultsEvent $event
   *   The event.
   */
  public function onSearchApiProcessingResults(Event $event) {
    /** @var \Drupal\search_api\Query\ResultSet $results */
    $results = $event->getResults();
    if (!empty($results->getResultItems())) {
      return;
    }
    $query = $results->getQuery();
    $index = $query->getIndex();
    $response = $results->getExtraData('elasticsearch_response');
    $suggestions = $response['suggest']['autocomplete'][0]['options'] ?? [];
    array_map(function ($item) use ($index, &$results) {
      $result_item = $this->fieldsHelper->createItem($index, $item['_id']);
      $result_item->setScore($item['_score']);
      $results->addResultItem($result_item);
    }, $suggestions);
  }

  /**
   * Get suggester values.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index name.
   * @param string $text
   *   The text.
   *
   * @return array
   *   The values.
   */
  protected function getSuggesterValues(IndexInterface $index, string $text): array {
    // Clone query to avoid changing the original query.
    $query = clone $index->query();
    // Get keys from query.
    $parts = &$query->getKeys();
    // Set query keys.
    $parts = explode(' ', $text);
    /** @var \Drupal\search_api\Processor\ProcessorInterface $processor */
    foreach ($index->getProcessors() as $processor) {
      // Run search processors in query keys.
      $processor->preprocessSearchQuery($query);
    }

    // Prepare processed value.
    $text = implode(' ', $parts);
    $item = (object) [
      'input' => $text,
      'weight' => 10,
    ];
    $value = [$item];
    $value[] = (object) [
      'input' => $parts,
      'weight' => 5,
    ];

    $pieces = [];
    while (!empty($parts)) {
      // Get parts from sentence.
      array_shift($parts);
      if (count($parts) > 1) {
        $pieces[] = implode(' ', $parts);
      }
    }

    $value[] = (object) [
      'input' => $pieces,
      'weight' => 3,
    ];

    return $value;
  }

  /**
   * Get autocomplete fields.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index name.
   *
   * @return array
   *   The fields.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFields(IndexInterface $index): array {
    $fields = [];

    // Load all autocomplete entities.
    $autocompletes = $this->entityTypeManager
      ->getStorage('search_api_autocomplete_search')
      ->loadMultiple();

    /** @var \Drupal\search_api_autocomplete\Entity\Search $value */
    foreach ($autocompletes as $value) {
      // Get autocomplete settings for the index.
      if ($value->getIndexId() == $index->id()) {
        /** @var \Drupal\search_api_autocomplete\Entity\Search $autocomplete */
        $autocomplete = $value;
        break;
      }
    }

    if (!isset($autocomplete)) {
      // No autocomplete settings for the index.
      return $fields;
    }

    /** @var \Drupal\search_api_autocomplete\Suggester\SuggesterInterface $suggester */
    foreach ($autocomplete->getSuggesters() as $suggester) {
      if ($suggester->getPluginId() != 'elastic_live_results') {
        // Not elasticsearch suggester.
        continue;
      }
      // Get fields for the suggester.
      $fields[] = $suggester->getConfiguration()['fields'] ?? [];
    }

    return array_merge(...$fields);
  }

  /**
   * Loads the index entity associated with this event.
   *
   * @param string $index_name
   *   The long index name as a string.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The loaded index or NULL.
   */
  protected function loadIndexFromIndexName($index_name):? EntityInterface {
    /** @var \Drupal\search_api\Entity\Index[] $search_api_indexes */
    $search_api_indexes = $this->entityTypeManager
      ->getStorage('search_api_index')
      ->loadMultiple();

    /** @var \Drupal\search_api\Entity\Index $search_api_index */
    foreach ($search_api_indexes as $search_api_index) {
      if ($index_name === $this->getIndexName($search_api_index)) {
        return $search_api_index;
      }
    }

    return NULL;
  }

  /**
   * Return the index name.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index.
   *
   * @return string
   *   The index name.
   */
  protected function getIndexName(IndexInterface $index): string {
    // Elasticsearch BackendClientInterface::getIndexId() method is not defined.
    // @phpstan-ignore-next-line
    return $index->getServerInstance()
      ->getBackend()
      ->getBackendClient()
      ->getIndexId($index);
  }

}
