<?php

namespace Drupal\elasticsearch_connector_suggester\Plugin\search_api\processor;

use Drupal\search_api\Plugin\search_api\processor\Tokenizer as SearchApiTokenizer;
use Drupal\search_api\Utility\Utility;

/**
 * Splits text into individual words for elasticsearch suggester searching.
 *
 * @SearchApiProcessor(
 *   id = "elasticsearch_connector_suggester_tokenizer",
 *   label = @Translation("Elasticsearch Connector Suggester Tokenizer"),
 *   description = @Translation("Splits text into individual words for elasticsearch suggester searching."),
 *   stages = {
 *     "pre_index_save" = 0,
 *     "preprocess_index" = -6,
 *     "preprocess_query" = -6
 *   }
 * )
 */
class Tokenizer extends SearchApiTokenizer {

  /**
   * {@inheritdoc}
   */
  protected function processFieldValue(&$value, $type) {
    $this->prepare();

    $text = $this->simplifyText($value);
    // Split on spaces. The configured (or default) delimiters have been
    // replaced by those already in simplifyText().
    $arr = explode(' ', $text);

    $value = [];
    foreach ($arr as $token) {
      if (is_numeric($token) || mb_strlen($token) >= $this->configuration['minimum_word_size']) {
        $value[] = Utility::createTextToken($token);
      }
    }
  }

  /**
   * @inheritDoc
   */
  protected function process(&$value) {
    $this->prepare();
    $value = trim($this->simplifyText($value));

    $min = $this->configuration['minimum_word_size'];
    if ($min > 1) {
      $words = explode(' ', $value);
      foreach ($words as $i => $word) {
        // Remove words that are not numeric and too short.
        if (!is_numeric($word) && mb_strlen($word) < $min) {
          unset($words[$i]);
        }
      }
      $value = implode(' ', $words);
    }
  }

}
