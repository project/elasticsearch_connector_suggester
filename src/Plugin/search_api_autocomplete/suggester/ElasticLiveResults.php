<?php

namespace Drupal\elasticsearch_connector_suggester\Plugin\search_api_autocomplete\suggester;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_autocomplete\Plugin\search_api_autocomplete\suggester\LiveResults;
use Drupal\search_api_autocomplete\Suggestion\SuggestionFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a suggester plugin that displays live results.
 *
 * @SearchApiAutocompleteSuggester(
 *   id = "elastic_live_results",
 *   label = @Translation("Elastic display live results"),
 *   description = @Translation("Display live results from elastic search to
 *   visitors as they type."),
 * )
 */
class ElasticLiveResults extends LiveResults implements PluginFormInterface {

  /**
   * The field helper.
   *
   * @var \Drupal\search_api\Utility\FieldsHelperInterface
   */
  protected $fieldHelper;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'analyzer' => 'standard',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['analyzer'] = [
      '#type' => 'select',
      '#title' => $this->t('Analyzer'),
      '#description' => $this->t('The analyzer to use for the search.'),
      '#options' => [
        'standard' => $this->t('Standard'),
        'simple' => $this->t('Simple'),
        'whitespace' => $this->t('Whitespace'),
        'stop' => $this->t('Stop'),
        'keyword' => $this->t('Keyword'),
        'pattern' => $this->t('Pattern'),
        'fingerprint' => $this->t('Fingerprint'),
      ],
      '#default_value' => $this->configuration['analyzer'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->fieldHelper = $container->get('search_api.fields_helper');
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocompleteSuggestions(QueryInterface $query, $incomplete_key, $user_input) {
    $fulltext_fields = $this->configuration['fields'];
    $index = $query->getIndex();
    if ($fulltext_fields) {
      // Take care only to set fields that are still indexed fulltext fields.
      $index_fields = $index->getFulltextFields();
      $fulltext_fields = array_intersect($fulltext_fields, $index_fields);
      if ($fulltext_fields) {
        $query->setFulltextFields($fulltext_fields);
      }
      else {
        $args = [
          '@suggester' => $this->label(),
          '@search' => $this->getSearch()->label(),
          '@index' => $index->label(),
        ];
        $this->getLogger()->warning('Only invalid fulltext fields set for suggester "@suggester" in autocomplete settings for search "@search" on index "@index".', $args);
      }
    }
    $query->keys($user_input);

    try {
      /** @var \Drupal\search_api\Query\ResultSet $results */
      $results = $query->execute();
    }
    catch (SearchApiException $e) {
      // If the query fails, there's nothing we can do about that.
      return [];
    }

    $result_items = $results->getResultItems();
    if (empty($result_items)) {
      $extra_data = $results->getAllExtraData();
      $response_items = $extra_data['elasticsearch_response']['hits']['hits'] ?? [];
      if (empty($response_items)) {
        $response_items = $extra_data['elasticsearch_response']['suggest']['autocomplete'][0]['options'] ?? [];
      }
      $items = array_map(function($item) {
        return [
          '_id' => $item['_id'],
          '_score' => $item['_score'],
        ];
      }, $response_items);

      foreach ($items as $item) {
        $id = $item['_id'] ?? NULL;
        $result_item = $this->fieldHelper->createItem($index, $id);
        $result_item->setScore($item['_score']);
        $result_items[$id] = $result_item;
      }
    }

    $objects = $index->loadItemsMultiple(array_keys($result_items));
    $factory = new SuggestionFactory($user_input);

    $suggestions = [];
    $view_modes = $this->configuration['view_modes'];
    $suggest_keys = $this->configuration['suggest_keys'];
    $highlight_field = NULL;
    if ($this->isHighlightingEnabled()) {
      $highlight_field = $this->configuration['highlight']['field'];
    }

    foreach ($result_items as $item_id => $item) {
      // If the result object could not be loaded, there's little we can do
      // here.
      if (empty($objects[$item_id])) {
        continue;
      }

      /** @var \Drupal\Core\Entity\Plugin\DataType\EntityAdapter $object */
      $object = $objects[$item_id] ?? NULL;
      if (!$object) {
        continue;
      }
      $item->setOriginalObject($object);
      try {
        $datasource = $item->getDatasource();
      }
      catch (SearchApiException $e) {
        // This should almost never happen, but theoretically it could, so we
        // just skip the item if this happens.
        continue;
      }

      // Check whether the user has access to this item.
      if (!$item->getAccessResult()->isAllowed()) {
        continue;
      }

      // Can't include results that don't have a URL.
      $url = $datasource->getItemUrl($object);
      if (!$url) {
        continue;
      }

      $datasource_id = $item->getDatasourceId();
      $bundle = $datasource->getItemBundle($object);
      // Use highlighted field, if configured.
      if ($highlight_field) {
        $highlighted_fields = $item->getExtraData('highlighted_fields');
        if (isset($highlighted_fields[$highlight_field][0])) {
          $highlighted_field = [
            '#type' => 'markup',
            '#markup' => Xss::filterAdmin($highlighted_fields[$highlight_field][0]),
          ];
          $suggestions[] = $factory->createUrlSuggestion($url, NULL, $highlighted_field);
          continue;
        }
      }

      if ($suggest_keys) {
        // Return the item label as suggested keywords, if one can be retrieved.
        $label = $datasource->getItemLabel($object);
        $fields = $this->configuration['fields'] ?? [];
        $field = array_shift($fields);
        if (!empty($field)) {
          /** @var \Drupal\search_api\Item\Field $search_field */
          if ($search_field = $datasource->getIndex()->getField($field)) {
            /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
            $entity = $object->getEntity();
            $entity_field = $search_field->getPropertyPath();
            if ($entity instanceof EntityInterface && $entity->hasField($entity_field)) {
              $label = $entity->get($entity_field)->getString();
            }
          }
        }
        if ($label) {
          $suggestions[] = $factory->createFromSuggestedKeys($label);
        }
      }
      elseif (empty($view_modes[$datasource_id][$bundle])) {
        // If no view mode was selected for this bundle, just use the label.
        $label = $datasource->getItemLabel($object);
        $suggestions[] = $factory->createUrlSuggestion($url, $label);
      }
      else {
        $view_mode = $view_modes[$datasource_id][$bundle];
        $render = $datasource->viewItem($object, $view_mode);
        if ($render) {
          // Add the excerpt to the render array to allow adding it to view
          // modes.
          $render['#search_api_excerpt'] = $item->getExcerpt();
          $suggestions[] = $factory->createUrlSuggestion($url, NULL, $render);
        }
      }
    }

    return $suggestions;
  }

}
