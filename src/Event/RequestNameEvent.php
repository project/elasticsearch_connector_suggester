<?php

namespace Drupal\elasticsearch_connector_suggester\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a name is requested.
 *
 * This event allows you to modify the route name before it is used.
 * This event is fired before elasticsearch query body changes the suggester.
 */
class RequestNameEvent extends Event {

  /**
   * The name.
   *
   * @var string
   */
  protected string $name;

  /**
   * Constructs a new RequestNameEvent.
   *
   * @param string $name
   *   The name.
   */
  public function __construct(string $name) {
    $this->name = $name;
  }

  /**
   * Gets the name.
   *
   * @return string
   *   The name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param string $name
   *   The name.
   */
  public function setName(string $name): void {
    $this->name = $name;
  }

}
