# Elasticsearch Connector Suggester

This module adds the ability to use elastixsearch suggester, which improves
suggester results for search_api, elasticsearch_connector and
search_api_autocomplete module.

When configuring autocomplete with search_api module and the
elasticsearch_connector module, autocompletion is a little clunky.

This module adds an elasticsearch suggester that can be applied to fields wich
the indexe's autocomplete is enabled.

To learn more about suggester in elasticsearch,
read here:
https://www.elastic.co/guide/en/elasticsearch/reference/master/search-suggesters.html

## Similar module
https://www.drupal.org/project/search_api_autocomp

## Requirements
* Search API module
* Search API Autocomplete module
* Elasticsearch Connector module

For 1.x version: Elasticsearch 7.x and Elasticsearch Connector 8.x-7.x versions.

For 2.x version: Elasticsearch 8.x and Elasticsearch Connector 8.0.x versions.

## Installation
Install as usual, see
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

## Configuration
1. Enable the module
2. Go to the search api index you want to add the suggester
3. Go to Processors tab
4. Select the 'Elasticsearch Connector Suggester Tokenizer' processor
5. Configure the settings for tokenizer.
6. Go to Autocomplete tab
7. Select the 'Elastic display live results' suggester
8. Select the field or fields you want to use for the suggester
9. Save the autocomplete configuration

## Author
Dudás József (https://www.drupal.org/u/dj1999)

## Supporting organization
This module was developed by Brainsum (https://www.drupal.org/brainsum)
